<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Question;
use App\User;


class QuestionsController extends Controller
{

    public function __construct(){
        $this->middleware(['auth'])->only(['create', 'store', 'edit', 'update']);
    }

    public function index()
    {
        $questions = Question::with('owner')->latest()->paginate(10); //eager load
        return view('/questions.index', compact([
            'questions'
        ]));
    }

    public function create()
    {
        app('debugbar')->disable();
        return view('questions.create');
    }

    public function store(CreateQuestionRequest $request)
    {
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);
        session()->flash('success', "Questions has been Added Successfully!");
        return redirect(route('questions.index'));
    }

    public function show(Question $question)
    {
        $question->increment('views_count');
        return view('questions.show', compact([
            'question'
        ]));
    }

    public function edit(Question $question)
    {

        if($this->authorize('update', $question))
        {
            app('debugbar')->disable();
            return view('questions.edit', compact([
                'question'
            ]));
        }
        abort(403);
        // if(Gate::allows('update-question', $question))
        // {
        //     app('debugbar')->disable();
        //     return view('questions.edit', compact([
        //         'question'
        //     ]));
        // }
        // abort(403);
    }

    public function update(UpdateQuestionRequest $request, Question $question)
    {
        // if(Gate::allows('update-question', $question))
        // {
        //     $question->update([
        //         'title' => $request->title,
        //         'body' => $request->body
        //     ]);
        //     session()->flash('success', "Questions has been Updated Successfully!");
        //     return redirect(route('questions.index'));
        // }
        // abort(403);

        if($this->authorize('update', $question))
        {
            $question->update([
                'title' => $request->title,
                'body' => $request->body
            ]);
            session()->flash('success', "Questions has been Updated Successfully!");
            return redirect(route('questions.index'));
        }
            abort(403);
    }

    public function destroy(Question $question)
    {
        // if(Gate::allows('delete-question', $question))
        // {
        //     $question->delete();
        //     session()->flash('success', "Questions has been Deleted Successfully!");
        //     return redirect(route('questions.index'));
        // }
        // abort(403);
        //Another Way To Do This-----
        // if(auth()->user()->can('delete-question', $question))
        // {
        //     $question->delete();
        //     session()->flash('success', "Questions has been Deleted Successfully!");
        //     return redirect(route('questions.index'));   
        // }
        // abort(403);

        if($this->authorize('delete', $question))
        {
            $question->delete();
            session()->flash('success', "Questions has been Deleted Successfully!");
            return redirect(route('questions.index'));   
        }
        abort(403);
    }
}
