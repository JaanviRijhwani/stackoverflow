<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'title' => rtrim($faker->sentence(rand(5, 10)), '.'),
        'body' => $faker->paragraphs(rand(3,7), true),
        'views_count'=> rand(0,10),// Eloquent(means model) Even Handling! Events like - retrieved, creating, created, updating, updated, saving, saved, storing, stored, deleting, delete, restored, restoring
        //Creating-> save and creating call hoga
        //Updating-> save and updating call hoga
        'votes_count' => rand(-10, 10)
    ];
    //To use Eloquent Event
    //We need to register the event in model
    //Question pe answer ko increment karna hai toh data answers se ayega so go to Answer Model
    // IN Model -static method isliye reate ni hoga toh bhi call hoga!
});
